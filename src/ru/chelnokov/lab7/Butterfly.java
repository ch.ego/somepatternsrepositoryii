package ru.chelnokov.lab7;

import ru.chelnokov.lab7.phases.*;

public class Butterfly {
    Phase phase;

    public void setPhase(Phase phase) {
        this.phase = phase;
    }

    public void changeActivity() {
        if (phase instanceof Caterpillar){
            setPhase(new Сhrysalis());
        }else if (phase instanceof Сhrysalis){
            setPhase(new Reborn());
        }else if (phase instanceof Reborn){
            setPhase(new Dying());
        }else if (phase instanceof Dying){
            setPhase(new Caterpillar());
        }
    }

    public void evolve(){
        phase.evolve();
    }
}


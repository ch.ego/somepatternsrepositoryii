package ru.chelnokov.lab7.phases;

public class Reborn implements Phase {
    @Override
    public void evolve() {
        System.out.println("Бабочка вылезла из кокона и летает");
    }
}

package ru.chelnokov.lab7.phases;

public interface Phase {
    void evolve();
}

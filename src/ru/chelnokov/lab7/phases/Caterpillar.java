package ru.chelnokov.lab7.phases;

public class Caterpillar implements Phase {
    @Override
    public void evolve() {
        System.out.println("Из яйца вылезла гусеница, она умеет только ползать");
    }
}

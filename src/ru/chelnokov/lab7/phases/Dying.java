package ru.chelnokov.lab7.phases;

public class Dying implements Phase {
    @Override
    public void evolve() {
        System.out.println("Грустно, но бабочка отлетала своё. Но мы знаем, где она оставила детей)");
    }
}

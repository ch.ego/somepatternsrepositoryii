package ru.chelnokov.lab7;

import ru.chelnokov.lab7.phases.Phase;
import ru.chelnokov.lab7.phases.Caterpillar;

public class LifecycleDemo {
    public static void main(String[] args) {
        Phase activity = new Caterpillar();
        Butterfly butterfly = new Butterfly();
        butterfly.setPhase(activity);

        for (int i = 0; i < 10; i++) {
            butterfly.evolve();
            butterfly.changeActivity();
        }
    }
}

package ru.chelnokov.lab5;

public class Subscriber implements Observer{
    String name;

    public Subscriber(String name) {
        this.name = name;
    }

    @Override
    public void handleEvent(String message) {
        System.out.println(name + " принял:\n" + message);
    }
}

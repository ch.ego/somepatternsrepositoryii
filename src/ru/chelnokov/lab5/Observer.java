package ru.chelnokov.lab5;

public interface Observer {
    void handleEvent(String message);
}

package ru.chelnokov.lab5;

import java.util.ArrayList;
import java.util.List;

public class Publisher implements Observed {
    String message;
    List<Observer> subs = new ArrayList<>();

    void setMessage(String message) {
        this.message = message;
        notifyObservers();
    }

    @Override
    public void registerSubscriber(Observer observer) {
        this.subs.add(observer);
    }

    @Override
    public void removeSubscriber(Observer observer) {
        this.subs.remove(observer);
    }

    @Override
    public void notifyObservers() {
        subs.forEach(s->{
            s.handleEvent(this.message);
        });
    }
}

package ru.chelnokov.lab5;

public interface Observed {
    void registerSubscriber(Observer observer);
    void removeSubscriber(Observer observer);
    void notifyObservers();
}

package ru.chelnokov.lab5;

public class Demo {
    public static void main(String[] args) {
        Publisher publisher = new Publisher();

        Observer subscriber1 = new Subscriber("Egor Chelnokov");
        Observer subscriber2 = new Subscriber("Ch.Ego");
        Observer subscriber3 = new Subscriber("vd1369");

        publisher.registerSubscriber(subscriber1);
        publisher.registerSubscriber(subscriber2);

        publisher.setMessage("hi");
        publisher.registerSubscriber(subscriber3);

        publisher.setMessage("THE END IS NEAR!");
        publisher.removeSubscriber(subscriber1);

        publisher.setMessage("He-he...");
    }
}

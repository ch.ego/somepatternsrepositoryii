package ru.chelnokov.lab9;

import ru.chelnokov.lab9.payment.Banknotes;
import ru.chelnokov.lab9.payment.Card;
import ru.chelnokov.lab9.payment.Coins;
import ru.chelnokov.lab9.payment.Transfer;

import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        Purchaser purchaser = new Purchaser();
        choosePayment(purchaser);
    }

    private static void choosePayment(Purchaser purchaser) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Выберите способ оплаты (введите цифру):\n" +
                "1) Купюры\n" +
                "2) Мелочь\n" +
                "3) Картой\n" +
                "4) Переводом\n" +
                "5) Отменить\n");

        int option = reader.nextInt();
        switch (option) {
            case 1:
                purchaser.setPayment(new Banknotes());
                purchaser.purchase();
                break;
            case 2:
                purchaser.setPayment(new Coins());
                purchaser.purchase();
                break;
            case 3:
                purchaser.setPayment(new Card());
                purchaser.purchase();
                break;
            case 4:
                purchaser.setPayment(new Transfer());
                purchaser.purchase();
                break;
            case 5:
                System.out.println("Действие отменено");
                break;
            default:
                System.out.println("Попробуйте ещё раз");
                choosePayment(purchaser);
                break;
        }
    }
}

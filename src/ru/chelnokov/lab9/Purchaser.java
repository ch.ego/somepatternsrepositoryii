package ru.chelnokov.lab9;

import ru.chelnokov.lab9.payment.Payment;

public class Purchaser {
    Payment payment;

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
    public void purchase(){
        payment.pay();
    }
}

package ru.chelnokov.lab8.templates;

public enum Color {
    WHITE("Белый"),
    BLUE("Синий"),
    RED("Красный"),
    BLACK("Черный"),
    GOLD("Золотой");

    private final String NAME;
    Color(String name) {
        this.NAME = name;
    }

    public String getNAME() {
        return NAME;
    }
}

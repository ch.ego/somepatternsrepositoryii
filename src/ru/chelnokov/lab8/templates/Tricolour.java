package ru.chelnokov.lab8.templates;

public class Tricolour {
    String countryName;
    Color first;
    Color second;
    Color third;

    public Tricolour(String countryName, Color first, Color second, Color third) {
        this.countryName = countryName;
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public void print() {
        System.out.printf("%s:%n%s%n%s%n%s%n%n", countryName, first.getNAME(), second.getNAME(), third.getNAME());
    }
}

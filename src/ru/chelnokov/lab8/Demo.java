package ru.chelnokov.lab8;

import ru.chelnokov.lab8.templates.GermanTemplate;
import ru.chelnokov.lab8.templates.RussianTemplate;
import ru.chelnokov.lab8.templates.Tricolour;

public class Demo {
    public static void main(String[] args) {
        Tricolour russian = new RussianTemplate();
        russian.print();
        Tricolour german = new GermanTemplate();
        german.print();
    }
}

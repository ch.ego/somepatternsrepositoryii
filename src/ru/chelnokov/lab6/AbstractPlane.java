package ru.chelnokov.lab6;

public abstract class AbstractPlane {
    protected Mediator mediator;
    protected String name;

    public AbstractPlane(Mediator mediator, String name) {
        this.mediator = mediator;
        this.name = name;
    }

    public abstract void sendLanding();
    public abstract void sendTakeOff();

    public abstract void receive(String msg);
}

package ru.chelnokov.lab6;

public class Plane extends AbstractPlane {
    String message;

    public Plane(Mediator med, String name) {
        super(med, name);
    }

    @Override
    public void sendLanding() {
        message = this.name + " заходит на посадку";
        sendMessage(message);
    }

    @Override
    public void sendTakeOff() {
        message = this.name + " начинает взлёт";
        sendMessage(message);
    }

    private void sendMessage(String message) {
        System.out.println(message);
        mediator.sendMessage(message, this);
    }

    @Override
    public void receive(String msg) {
        System.out.println(this.name + " принял: " + msg);
    }

}
package ru.chelnokov.lab6;

import java.util.ArrayList;
import java.util.List;

public class Operator implements Mediator {

    private List<AbstractPlane> users;

    public Operator() {
        this.users = new ArrayList<>();
    }

    @Override
    public void addUser(AbstractPlane user) {
        this.users.add(user);
    }

    @Override
    public void sendMessage(String msg, AbstractPlane user) {
        for (AbstractPlane plane : this.users) {
            if (plane != user) {
                plane.receive(msg);
            }
        }
    }

}
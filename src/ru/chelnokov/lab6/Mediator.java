package ru.chelnokov.lab6;

public interface Mediator {

    void sendMessage(String msg, AbstractPlane user);

    void addUser(AbstractPlane user);
}
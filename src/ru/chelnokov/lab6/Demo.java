package ru.chelnokov.lab6;

public class Demo {
    public static void main(String[] args) {
        Mediator mediator = new Operator();
        Plane plane1 = new Plane(mediator, "Plane1");
        Plane plane2 = new Plane(mediator, "Plane2");
        Plane plane3 = new Plane(mediator, "Plane3");
        Plane plane4 = new Plane(mediator, "Plane4");
        mediator.addUser(plane1);
        mediator.addUser(plane2);
        mediator.addUser(plane3);
        mediator.addUser(plane4);

        plane1.sendTakeOff();
        plane3.sendLanding();
    }
}

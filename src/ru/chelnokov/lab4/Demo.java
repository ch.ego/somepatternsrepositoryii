package ru.chelnokov.lab4;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        ArrayList<String> todo = new ArrayList<>(List.of("Сходить в парикмахерскую", "Позвонить сестре", "Сделать лабу"));
        DayTasks dayTasks = new DayTasks("Пятница 11.02", todo);
        Iterator iterator = dayTasks.getIterator();

        System.out.println("Сегодня: " + dayTasks.getDay());
        System.out.println("План на день:");

        while (iterator.hasNext()) {
            System.out.println(iterator.getNext().toString());
        }
    }
}

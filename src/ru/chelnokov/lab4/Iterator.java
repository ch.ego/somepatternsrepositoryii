package ru.chelnokov.lab4;

public interface Iterator {
    boolean hasNext();

    Object getNext();
}

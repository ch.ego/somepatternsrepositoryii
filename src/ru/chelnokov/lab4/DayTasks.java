package ru.chelnokov.lab4;

import java.util.ArrayList;

public class DayTasks implements Collection {
    String day;
    ArrayList<String> tasks;

    public DayTasks(String day, ArrayList<String> tasks) {
        this.day = day;
        this.tasks = tasks;
    }

    @Override
    public Iterator getIterator() {
        return new TasksIterator();
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public ArrayList<String> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<String> tasks) {
        this.tasks = tasks;
    }

    private class TasksIterator implements Iterator {

        int index;

        @Override
        public boolean hasNext() {
            if (index < tasks.size()) {
                return true;
            }
            return false;
        }

        @Override
        public Object getNext() {
            return tasks.get(index++);
        }
    }
}

package ru.chelnokov.lab4;

public interface Collection {
    Iterator getIterator();
}

package ru.chelnokov.lab2.shapes;

/**
 * Ромб - это частный случай параллелограмма с равными сторонами
 */
public class Rhomb extends Parallelogram implements CloneableShape {
    public Rhomb(int width, int angle) {
        super(width, width, angle);
    }

    public Rhomb(Rhomb target) {
        super(target);
    }

    @Override
    public Rhomb clone() {
        return new Rhomb(this);
    }

    @Override
    public boolean equals(Object object2) {
        if (!(object2 instanceof Rhomb)) return false;
        Rhomb rhomb2 = (Rhomb) object2;
        return rhomb2.width == width && rhomb2.angle == angle;
    }
}

package ru.chelnokov.lab2.shapes;

/**
 * По сути, любой симметричный четырехугольник
 */
public class Parallelogram implements CloneableShape {
    int width;
    int height;
    int angle;

    public Parallelogram(int width, int height, int angle) {
        this.width = width;
        this.height = height;
        this.angle = getAngle(angle);
    }

    public Parallelogram(Parallelogram target) {
        if (target != null) {
            this.width = target.width;
            this.height = target.height;
            this.angle = getAngle(target.angle);
        }
    }

    int getAngle(int value) {
        while (value > 180) {
            value -= 180;
        }
        return value;
    }

    @Override
    public Parallelogram clone() {
        return new Parallelogram(this);
    }

    @Override
    public boolean equals(Object object2) {
        if (!(object2 instanceof Parallelogram)) return false;
        Parallelogram pgram2 = (Parallelogram) object2;
        return pgram2.width == width && pgram2.height == height && pgram2.angle == angle;
    }
}

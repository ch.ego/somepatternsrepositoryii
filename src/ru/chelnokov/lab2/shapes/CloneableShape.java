package ru.chelnokov.lab2.shapes;

public interface CloneableShape {
    Parallelogram clone();
}

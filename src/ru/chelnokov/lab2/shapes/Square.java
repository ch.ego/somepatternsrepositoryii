package ru.chelnokov.lab2.shapes;

/**
 * Квадрат - это частный случай прямоугольника с равными сторонами
 */
public class Square extends Rectangle implements CloneableShape {
    public Square(int width) {
        super(width, width);
    }

    public Square(Square target) {
        super(target);
    }

    @Override
    public Square clone() {
        return new Square(this);
    }

    @Override
    public boolean equals(Object object2) {
        if (!(object2 instanceof Square)) return false;
        Square square2 = (Square) object2;
        return square2.width == width;
    }
}

package ru.chelnokov.lab2.shapes;

/**
 * Прямоугольник это частный случай параллелограмма, угол которого равен 90°
 */
public class Rectangle extends Parallelogram implements CloneableShape {
    public Rectangle(int width, int height) {
        super(width, height, 90);
    }

    public Rectangle(Rectangle target) {
        super(target);
    }

    @Override
    public Rectangle clone() {
        return new Rectangle(this);
    }

    @Override
    public boolean equals(Object object2) {
        if (!(object2 instanceof Rectangle)) return false;
        Rectangle rec2 = (Rectangle) object2;
        return rec2.width == width && rec2.height == height;
    }
}

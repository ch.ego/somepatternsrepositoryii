package ru.chelnokov.lab2;

import ru.chelnokov.lab2.shapes.Rhomb;

public class Demo {
    public static void main(String[] args) {
        Rhomb rhomb = new Rhomb(5, 33);
        Rhomb cloned = rhomb.clone();
        System.out.println(rhomb.equals(cloned));
    }
}

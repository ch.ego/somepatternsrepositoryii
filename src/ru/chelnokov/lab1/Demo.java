package ru.chelnokov.lab1;

import ru.chelnokov.lab1.coffee.Coffee;
import ru.chelnokov.lab1.coffee.CoffeeFactory;
import ru.chelnokov.lab1.coffee.CoffeeShop;
import ru.chelnokov.lab1.coffee.CoffeeType;

public class Demo {
    public static void main(String[] args) {
        CoffeeShop shop = new CoffeeShop(new CoffeeFactory());
        Coffee coffee = shop.orderCoffee(CoffeeType.AMERICANO);
        System.out.printf("Ура, я заказал %s!", coffee.getNAME());
    }
}

package ru.chelnokov.lab1.coffee;

public class Coffee {
    private final String NAME;

    public void grindCoffee() {
        // перемалываем кофе
    }

    public void makeCoffee() {
        // делаем кофе
    }

    public void pourIntoCup() {
        // наливаем в чашку
    }

    protected Coffee(String name) {
        this.NAME = name;
    }

    public String getNAME() {
        return NAME;
    }
}

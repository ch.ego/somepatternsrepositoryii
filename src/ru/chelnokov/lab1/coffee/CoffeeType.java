package ru.chelnokov.lab1.coffee;

import ru.chelnokov.lab1.types.Americano;
import ru.chelnokov.lab1.types.CaffeLatte;
import ru.chelnokov.lab1.types.Cappucino;
import ru.chelnokov.lab1.types.Espresso;

public enum CoffeeType {
    ESPRESSO,
    AMERICANO,
    CAFFE_LATTE,
    CAPPUCCINO;
}

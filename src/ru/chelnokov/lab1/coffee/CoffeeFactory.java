package ru.chelnokov.lab1.coffee;

import ru.chelnokov.lab1.types.Americano;
import ru.chelnokov.lab1.types.CaffeLatte;
import ru.chelnokov.lab1.types.Cappucino;
import ru.chelnokov.lab1.types.Espresso;

public class CoffeeFactory {
    public Coffee createCoffee(CoffeeType type) {
        switch (type) {
            case ESPRESSO:
                return new Espresso();
            case AMERICANO:
                return new Americano();
            case CAPPUCCINO:
                return new Cappucino();
            case CAFFE_LATTE:
                return new CaffeLatte();
            default:
                throw new NullPointerException();// :)
        }
    }
}

package ru.chelnokov.lab1.types;

import ru.chelnokov.lab1.coffee.Coffee;

public class Cappucino extends Coffee {
    public Cappucino() {
        super("Капучино");
    }
}

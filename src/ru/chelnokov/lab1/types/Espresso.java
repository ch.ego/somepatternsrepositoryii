package ru.chelnokov.lab1.types;

import ru.chelnokov.lab1.coffee.Coffee;

public class Espresso extends Coffee {
    public Espresso() {
        super("Эспрессо");
    }
}

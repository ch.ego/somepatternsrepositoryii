package ru.chelnokov.lab1.types;

import ru.chelnokov.lab1.coffee.Coffee;

public class Americano extends Coffee {
    public Americano() {
        super("Американо");
    }
}

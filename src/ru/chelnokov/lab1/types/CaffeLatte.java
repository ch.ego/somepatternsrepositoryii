package ru.chelnokov.lab1.types;

import ru.chelnokov.lab1.coffee.Coffee;

public class CaffeLatte extends Coffee {
    public CaffeLatte() {
        super("Латте");
    }
}

package ru.chelnokov.lab3.builders;

import ru.chelnokov.lab3.cup.MaterialType;

public class PlasticCupBuilder extends CupBuilder {
    @Override
    public void setManufacturer() {
        cup.setManufacturer("Компания “НАПРА”");
    }

    @Override
    public void setMaterial() {
        cup.setMaterial(MaterialType.PLASTIC);
    }

    @Override
    public void setPrice() {
        cup.setPrice(0.50);
    }
}
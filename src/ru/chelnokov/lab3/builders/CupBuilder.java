package ru.chelnokov.lab3.builders;

import ru.chelnokov.lab3.cup.Cup;

public abstract class CupBuilder {
    Cup cup;

    public void createCup() {
        cup = new Cup();
    }

    public abstract void setManufacturer();

    public abstract void setMaterial();

    public abstract void setPrice();

    public Cup getCup() {
        return cup;
    }
}

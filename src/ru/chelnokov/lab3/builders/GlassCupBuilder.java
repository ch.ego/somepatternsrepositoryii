package ru.chelnokov.lab3.builders;

import ru.chelnokov.lab3.cup.MaterialType;

public class GlassCupBuilder extends CupBuilder {
    @Override
    public void setManufacturer() {
        cup.setManufacturer("Уршельский завод");
    }

    @Override
    public void setMaterial() {
        cup.setMaterial(MaterialType.GLASS);
    }

    @Override
    public void setPrice() {
        cup.setPrice(20.00);
    }
}

package ru.chelnokov.lab3.builders;

import ru.chelnokov.lab3.cup.MaterialType;

public class CrystalCupBuilder extends CupBuilder {
    @Override
    public void setManufacturer() {
        cup.setManufacturer("Гусевской хрустальный завод");
    }

    @Override
    public void setMaterial() {
        cup.setMaterial(MaterialType.CRYSTAL);
    }

    @Override
    public void setPrice() {
        cup.setPrice(490.00);
    }
}
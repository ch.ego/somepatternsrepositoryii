package ru.chelnokov.lab3.director;

import ru.chelnokov.lab3.builders.CupBuilder;
import ru.chelnokov.lab3.cup.Cup;

public class Director {
    CupBuilder builder;

    public void setBuilder(CupBuilder builder) {
        this.builder = builder;
    }

    public Cup buildCup() {
        builder.createCup();
        builder.setManufacturer();
        builder.setMaterial();
        builder.setPrice();
        return builder.getCup();
    }
}

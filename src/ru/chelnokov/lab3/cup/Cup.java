package ru.chelnokov.lab3.cup;

public class Cup {
    private String manufacturer;
    private MaterialType type;
    private double price;

    public String getManufacturer() {
        return manufacturer;
    }

    public MaterialType getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setMaterial(MaterialType type) {
        this.type = type;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}

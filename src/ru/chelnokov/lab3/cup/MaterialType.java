package ru.chelnokov.lab3.cup;

public enum MaterialType {
    GLASS,
    CRYSTAL,
    PLASTIC;
}

package ru.chelnokov.lab3;

import ru.chelnokov.lab3.builders.CrystalCupBuilder;
import ru.chelnokov.lab3.cup.Cup;
import ru.chelnokov.lab3.director.Director;

public class Demo {
    public static void main(String[] args) {
        Director director = new Director();
        director.setBuilder(new CrystalCupBuilder());
        Cup cup = director.buildCup();
        System.out.println(cup.getManufacturer());
    }
}
